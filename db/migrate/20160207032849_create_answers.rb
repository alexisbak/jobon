class CreateAnswers < ActiveRecord::Migration
  def change
    create_table :answers do |t|
      t.string :asset_file_name
      t.string :asset_content_type

      t.timestamps null: false
    end
  end
end
